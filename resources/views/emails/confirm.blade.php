@component('mail::message')
# Hello {{$user->name}}

You have changed your email. Please confirm the new email address with the link below

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent